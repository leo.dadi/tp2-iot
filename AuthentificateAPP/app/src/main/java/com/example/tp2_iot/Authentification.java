package com.example.tp2_iot;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Authentification extends AppCompatActivity {

    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),
                1024);//ww w  . ja v  a 2 s . c  om
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);
        //get variables from xml file
        Button b =(Button)findViewById(R.id.authentificate);
        EditText login = (EditText)findViewById(R.id.login);
        EditText password = (EditText)findViewById(R.id.password);
        TextView resultView = (TextView) findViewById(R.id.result);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        URL url = null;
                        try {
                            //get values needed
                            String textLogin = login.getText().toString();
                            String textPassword = password.getText().toString();
                            String login_password = textLogin + ":" + textPassword;
                            url = new URL("https://httpbin.org/basic-auth/bob/sympa");
                            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                            String basicAuth = "Basic " + Base64.encodeToString(
                                    login_password.getBytes(),
                                    Base64.NO_WRAP);
                            Log.i("String", basicAuth);
                            urlConnection.setRequestProperty("Authorization", basicAuth);
                            try {
                                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                                String s = readStream(in);
                                Log.i("JFL", s);
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        // We refresh the result TextView
                                        resultView.setText(s);
                                    }
                                });
                            } finally {
                                urlConnection.disconnect();
                            }
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
        }});
    }
}