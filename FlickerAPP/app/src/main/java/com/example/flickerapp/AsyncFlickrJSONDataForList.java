package com.example.flickerapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONDataForList extends AsyncTask<String, Void, JSONObject> {
    MyAdapter adapter;
    String url="https://www.flickr.com/services/feeds/photos_public.gne?tags=cats&format=json";
    public AsyncFlickrJSONDataForList(MyAdapter adapter) {
        this.adapter=adapter;
    }

    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),
                1024);//ww w  . ja v  a 2 s . c  om
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }
//nothing in the following code
    protected JSONObject doInBackground(String... strings) {
        try {
            URL url = new URL(this.url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = readStream(in);
                String jsonString = s.substring(15, s.length() - 1);
                JSONObject json = new JSONObject(jsonString);
                return json;
            } catch (JSONException e) {
                Log.i("JSONERROR","");
                throw new RuntimeException(e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(JSONObject result){
        try {
            //this time we want all URL so we take the JSONArray
            JSONArray jsonList= result.getJSONArray("items");
            //iterating over the lenght of the array, we put every URL into the adapter
            for(int i=0;i<jsonList.length(); i++) {
                //we extract the URL from the i-th item
                String url = jsonList.getJSONObject(i).getJSONObject("media").getString("m");
                adapter.add(url);
                Log.i("JFL", "Adding to adapter url : " + url);
            }
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        //to update the ListView the adapter is linked to
        adapter.notifyDataSetChanged();
    }
}
