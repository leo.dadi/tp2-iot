package com.example.flickerapp;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncFlickrJSONData extends AsyncTask<String, Void, JSONObject> {

    String url;
    public AsyncFlickrJSONData(String s) {
        this.url=s;

    }
//always need to add function readStream because it isn't there by fault
    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),
                1024);//ww w  . ja v  a 2 s . c  om
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    protected JSONObject doInBackground(String... strings) {
        try {
            //create the URL we will connect too
            URL url = new URL(this.url);
            //open connection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                //try to read the stream from url
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String s = readStream(in);
                //remove unnecessary beginning and trailing character to enable JSON conversion
                String jsonString = s.substring(15, s.length() - 1);
                JSONObject json = new JSONObject(jsonString);
                return json;
            } catch (JSONException e) {
                Log.i("JSONERROR","");
                throw new RuntimeException(e);
            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(JSONObject result){
        Log.i("Result", result.toString());
        try {
            //get image link from JSON
            String imageURL = result.getString("link");
            //finally it works
            Log.i("Finalky", imageURL);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }
}
