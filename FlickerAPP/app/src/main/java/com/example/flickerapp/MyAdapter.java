package com.example.flickerapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.widget.ContentFrameLayout;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class MyAdapter extends ArrayAdapter {

    public MyAdapter(Context context, ArrayList<String> vector) {
        super(context, 0, vector);
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        Log.i("JFL", "TODO");
        //always check if view if null, if that's the case we have to get it from the context
        if (view == null) {
            //getting the view
            //view = LayoutInflater.from(getContext()).inflate(R.layout.textview, parent, false);
            view = LayoutInflater.from(getContext()).inflate(R.layout.bitmaplayout, parent, false);
        }
        //we just have to build and set the text in the TextView we created

        ImageView imageView = (ImageView) view.findViewById(R.id.bitmapimage);
        Response.Listener<Bitmap> rep_listener = response -> {
            imageView.setImageBitmap(response);
        };
        RequestQueue queue = MySingleton.getInstance(parent.getContext()).getRequestQueue();
        ImageRequest request = new ImageRequest(
                (String) getItem(i), rep_listener, 0,
                0, ImageView.ScaleType.CENTER_INSIDE, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error","Image Request failed");
            }

        });
        queue.add(request);
        return view;
    }
}
