package com.example.flickerapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AsyncBitmapDownloader extends AsyncTask<String, Void, Bitmap> {
    String url;
    ImageView image;
    public AsyncBitmapDownloader(String s, ImageView image) {
        this.url = s;
        this.image = image;
    }
    public static String readStream(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader r = new BufferedReader(new InputStreamReader(in),
                1024);//ww w  . ja v  a 2 s . c  om
        for (String line = r.readLine(); line != null; line = r.readLine()) {
            sb.append(line);
        }
        in.close();
        return sb.toString();
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        try {
            //create JSON url
            URL fetchURL = new URL(url);
            Log.i("url", url);
            HttpURLConnection urlConnection = (HttpURLConnection) fetchURL.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String s = readStream(in);
            String jsonString = s.substring(15, s.length() - 1);
            JSONObject json = new JSONObject(jsonString);
            //so far we have the JSON file, we now need to extract the image, so another URL connection from the correct link
            JSONObject item = json.getJSONArray("items").getJSONObject(1);
            JSONObject media = item.getJSONObject("media");
            //disconnect first connection
            urlConnection.disconnect();
            //open connection to correct image URL extracted from previous JSON
            fetchURL = new URL(media.getString("m"));
            urlConnection = (HttpURLConnection) fetchURL.openConnection();
            InputStream image_stream = new BufferedInputStream(urlConnection.getInputStream());
            Bitmap bm = BitmapFactory.decodeStream(image_stream);
            urlConnection.disconnect();
            return bm;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return null;
    }
    @Override
    protected void onPostExecute(Bitmap bm){

        //set downloaded image into the ImageView passed as a parameter.
        image.setImageBitmap(bm);
    }
}
