package com.example.flickerapp;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONObject;

public class GetImageOnClickListener implements View.OnClickListener {
    ImageView image;
    public GetImageOnClickListener(ImageView image) {
        this.image = image;
    }


    @Override
    public void onClick(View view) {
        Log.i("ImageonClick", "yes");

        AsyncTask<String, Void, Bitmap> async = new AsyncBitmapDownloader("https://www.flickr.com/services/feeds/photos_public.gne?tags=trees&format=json",image).execute();
    }
}
