package com.example.flickerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.ListView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        //ListView that will be populated
        ListView listView = (ListView) findViewById(R.id.listView);
        //Empty Array list to use a parameter inthe constructor
        ArrayList<String> vec = new ArrayList<String>();
        //created the adapter, passing it the context and the empty list
        MyAdapter adapter = new MyAdapter(this, vec);
        //linking the adapter to the listView
        listView.setAdapter(adapter);
        //updating the adapter data
        AsyncTask<String, Void, JSONObject> test = new AsyncFlickrJSONDataForList(adapter).execute();


    }
}